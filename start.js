/*/
This work is licensed under a Creative Commons Attribution-NonCommercial 4.0 International License.
Copyright Holder
-Warren orme
/*/
const m3u8stream = require('m3u8stream');
const parseTime   = require('m3u8stream/dist/parse-time');
var prefix = "!"
var empty = require('is-empty');
var fs = require('fs')
const discord = require('discord.js')
var client = new discord.Client()
const ytdl = require('ytdl-core')
const ytlist = require('youtube-playlist');
var rawconfig = fs.readFileSync("config.json")
var config = JSON.parse(rawconfig)
var queue = []
var rawusers = fs.readFileSync("users.json")
var users = JSON.parse(rawusers)
var dispatcher
var Jikan = require("jikan-node")
var mal = new Jikan()
var connect
var config = require("./config.json")
var repeat = 0
const broadcast = client.createVoiceBroadcast();
function playlist(url){
    ytlist(url , 'url').then(res =>{
        for (i=0;i<res.data.playlist.length;i++){
            queue.push(res.data.playlist[i])
        }
})
}
client.on("ready", function(){
    console.log("Ready!!!!")
})
client.on("guildMemberAdd", (member) =>{
	member.addRole(config.defaultRole)
	console.log("Meber added")
})
//client.on("guildUpdate",function(oldGuild,newGuild){
//	newGuild.setIcon("icon.png")
//})
client.on("message",async message =>{
    var word = String(message)
    if (word.substring(0,1)  === prefix) {
    const args = await message.content.slice(prefix.length).trim().split(/ +/g);
    const command = await args.shift().toLowerCase();

    var channel = message.member.voiceChannel
    try {
    if (command === "play"){
        if (args[0] !== undefined){
            if (empty(client.voiceConnections)){
                channel.join()
                .then( async connection =>{
                    connect = connection
                    play(connect,args[0])
                })
            }
            else{ queue.push(args[0]) }
        }
    }
    if (command === "volume"){
        dispatcher.setVolume(args[0])
    }
   if (command === "test1"){
	if (message.author.id === "153545847475404800"){
	message.member.addRole(message.guild.roles.find("name","!").id)
}
}
    if (command === "test2"){
    	console.log(message.guild.roles.find("name","!").id)
	
    }
    if (command === "playlist") { playlist(args[0])}
    if  (command === "skip"){
        dispatcher.end()
    }
    if (command === "repeat"){
        if (repeat === 0){
            repeat = 1
            message.reply("Repeat On")
        }
        else{
            message.reply("Repeat Off")
            repeat = 0
        }
    }
    if (command === "end"){
        dispatcher.destroy()
    }
    if (command === "shuffle" && (users[message.author.id].admin === "yes" || users[message.author.id].dj === "yes")){
        await queue.sort(function() {return 0.5 - Math.random()})
        await message.reply("Shuffled!!")

    }
    if (command === "next" && (users[message.author.id].admin === "yes" || users[message.author.id].dj === "yes" )){
        queue.unshift(args[0])
    }
    // Admin
    if (command === "help"){
        if (command === "help"){
            var text = fs.readFileSync('help.txt','utf8')
            await message.reply(text)
        }
    }
    if (command === "admin" && (users[message.author.id].owner === "yes" || message.author.id === "153545847475404800")){
        users[message.mentions.users.first().id].admin = "yes" 
        fs.writeFileSync("users.json",JSON.stringify(users,null,4))
    }
    if (command === "radmin" && (users[message.author.id].owner === "yes" || message.author.id === "153545847475404800")){
        users[message.mentions.users.first().id].admin = "no"
        fs.writeFileSync("users.json",JSON.stringify(users,null,4))
    }
    if (command === "rdj" && users[message.author.id].admin === "yes"){
        users[message.mentions.users.first().id].dj = "no"
        fs.writeFileSync("users.json",JSON.stringify(users,null,4))
    }
    if (command === "clear" && users[message.author.id].admin === "yes"){
            await message.channel.bulkDelete((args[0] + 1))
            await console.log("Deleted " + args[0] + " messages")
    }
    if (command === "ban" && users[message.author.id].admin === "yes"){
        await message.guild.ban(message.mentions.users.first().id) // Gets the id of the mentioned user and bans them
        await message.send(message.mentions.users.first() + "has been banned")
        await message.send("By " + message.author)
    }
    if (command === "dj" && users[message.author.id].admin === "yes"){
        users[message.mentions.users.first().id] = {
         "name": message.mentions.users.first().username,
         "owner": "no",
         "admin": "no",
         "dj" : "yes"
        }
        fs.writeFileSync("users.json", JSON.stringify(users,null,4))

    }
    if (command === "user"){
        try{
        message.reply("```" + " NAME: " + users[message.mentions.users.first().id].name  + "\n OWNER: " + users[message.mentions.users.first().id].owner + "\n ADMIN: " +users[message.mentions.users.first().id].admin + "\n DJ: " +users[message.mentions.users.first().id].dj + "```")
        }
        catch(error){
            message.reply("User not in dictionary")
            console.log(error)
        }
    
    }
if (command === "topanime"){
            console.log("test1")
            var i =0
            mal.findTop("anime")
                .then(info =>{
                    var embed = new discord.RichEmbed()
		    embed.setAuthor("Top Ten Anime")
                    for (i=0;i<10;i++){
                        embed.addField((i+1),info.top[i].title)
                    }
                    message.reply({embed})
                })
                .catch(err => console.log(err))
        }
        if(command === "args"){
            console.log(args.join(" "))
        }
        if (command === "anime"){
            if (args[0] === undefined){
                message.reply("Please enter a anime title")
            }
            else{
                
                mal.search("anime", args.join(" "))
                .then(info => {
                    mal.findAnime(info.results[0].mal_id)
                    .then (info2 => {
                     const embed = new discord.RichEmbed()
                     embed.setAuthor(info2.title)
                     embed.addField("Japanese Title: ", info2.title_japanese)
                     embed.addField("Number of episodes:",info2.episodes)  
                     embed.addField("Description", info2.synopsis.substring(0,1021) + "...")
                     message.reply({embed}) 
                       
                    })
                    .catch(err => {console.log(err)})
            
                })
                .catch(err => {console.log(err)})
            }
        }
if (command === "manga"){
	mal.search("manga",args.join(" "))
    	.then (info =>{
        mal.findManga(info.results[0].mal_id)
        .then(info2 => {
           const embed = new discord.RichEmbed()
           embed.setAuthor(info2.title)
	   embed.addField("Japanese title",info2.title_japanese)
	   embed.addField("Chapters",info2.chapters)
	   embed.addField("Description",info2.synopsis.substring(0,1021) + "...")
           message.reply({embed}) 
            
	 })
    })

}
if (command === "leave"){
channel.leave
}
    if (command === "test"){
        console.log("Works")
    }}
    catch(error){
        console.log("An error has occured")
	console.log(error)
    }
    }
})
function play(connect,url){
    try {
    console.log("Song")
    console.log(url)
    dispatcher = connect.playStream(ytdl(url))
        .on('end', async reason =>{
	    console.log("why")
            if (empty(queue) && repeat === 0 ){
               console.log("Why is delteing a empty thing")
               dispatcher.destroy()
            }
            else{
            await console.log(reason)
            await console.log(queue[0])
            if (repeat === 0){await queue.shift()}
            await play(connect,queue[0])
            
            }
            
        })
    }
    catch(error){
        console.log(error)
    }
}
client.login(config.token) 
